/**
 * @author Luis Ángel Rodriguez Irisson
 * @author asd
 */

var Iss = {

  getVerticesFromBufferGeometry: function (buffer_geometry){
   var points = buffer_geometry.attributes.position.array;
   var vertices = [];
   for (var i = 0; i < points.length; i+=3) {
     vertices[((i+3)/3)-1] = new THREE.Vector3(
       points[i],
       points[i+1],
       points[i+2]);
   }
   return vertices;
 },

 computeScreenSpaceBoundingBox: function (mesh, camera) {
   var vertices = Iss.getVerticesFromBufferGeometry(mesh.geometry);
   var vertex = new THREE.Vector3();
   var min = new THREE.Vector3(1,1,1);
   var max = new THREE.Vector3(-1,-1,-1);
   var inc = Math.floor(vertices.length/50)+1;
   for (var i = 0; i < vertices.length-inc-1; i+=inc) {
     var vertexWorldCoord = vertex.copy(vertices[i]).applyMatrix4(mesh.matrixWorld);
     var vertexScreenSpace = vertexWorldCoord.project(camera);
     min.min(vertexScreenSpace);
     max.max(vertexScreenSpace);
   }
   return new THREE.Box2(min, max);
 },

 normalizedToPixels: function (coord, renderWidthPixels, renderHeightPixels) {
   var halfScreen = new THREE.Vector2(renderWidthPixels/2, renderHeightPixels/2)
   return coord.clone().multiply(halfScreen);
 },

 overlayBetweenTwoBboxes: function( obj1, obj2 ){
   var x_distance = Math.abs( obj1.position.x - obj2.position.x );
   var y_distance = Math.abs( obj1.position.y - obj2.position.y );

   var ov_x = x_distance / ((obj1.scale.x + obj2.scale.x)/2);
   var ov_y = y_distance / ((obj1.scale.y + obj2.scale.y)/2);

   var obj1_x1 = obj1.position.x-(obj1.scale.x/2);
   var obj1_x2 = obj1.position.x+(obj1.scale.x/2);
   var obj2_x1 = obj2.position.x-(obj2.scale.x/2);
   var obj2_x2 = obj2.position.x+(obj2.scale.x/2);

   var obj1_y1 = obj1.position.y-(obj1.scale.y/2);
   var obj1_y2 = obj1.position.y+(obj1.scale.y/2);
   var obj2_y1 = obj2.position.y-(obj2.scale.y/2);
   var obj2_y2 = obj2.position.y+(obj2.scale.y/2);

   if (ov_x<1) {
     var overlay_der_x = Math.abs(obj1_x1-obj2_x2);
     var overlay_izq_x = Math.abs(obj2_x1-obj1_x2);

     var overlay_x = Math.min(overlay_der_x,overlay_izq_x)/Math.min(obj1.scale.x, obj2.scale.x);
     //console.log('overlay_x: ' + overlay_x);
   }else {
     var overlay_x = 0;
   }
   if (ov_y<1) {
     var overlay_der_y = Math.abs(obj1_y1-obj2_y2);
     var overlay_izq_y = Math.abs(obj2_y1-obj1_y2);

     var overlay_y = Math.min(overlay_der_y,overlay_izq_y)/Math.min(obj1.scale.y, obj2.scale.y);
   }else {
     var overlay_y = 0;
   }
   return overlay_x*overlay_y;

 },

 overlayBetweenTwoCanvasBboxes: function( obj1, obj2 ){
   var obj1_scale_x = obj1.max.x-obj1.min.x;
   var obj1_scale_y = obj1.max.y-obj1.min.y;
   var obj2_scale_x = obj2.max.x-obj2.min.x;
   var obj2_scale_y = obj2.max.y-obj2.min.y;

   var obj1_position_x = obj1.min.x + obj1_scale_x/2;
   var obj1_position_y = obj1.min.y + obj1_scale_y/2;
   var obj2_position_x = obj2.min.x + obj2_scale_x/2;
   var obj2_position_y = obj2.min.y + obj2_scale_y/2;

   var x_distance = Math.abs( obj1_position_x - obj2_position_x );
   var y_distance = Math.abs( obj1_position_y - obj2_position_y );


   var ov_x = x_distance / ((obj1_scale_x + obj2_scale_x)/2);
   var ov_y = y_distance / ((obj1_scale_y + obj2_scale_y)/2);


   if (ov_x<1) {
     var overlay_der_x = Math.abs(obj1.min.x-obj2.max.x);
     var overlay_izq_x = Math.abs(obj2.min.x-obj1.max.x);

     var overlay_x = Math.min(overlay_der_x,overlay_izq_x)/Math.min(obj1_scale_x, obj2_scale_x);
     //console.log('overlay_x: ' + overlay_x);
   }else {
     var overlay_x = 0;
   }
   if (ov_y<1) {
     var overlay_der_y = Math.abs(obj1.min.y-obj2.max.y);
     var overlay_izq_y = Math.abs(obj2.min.y-obj1.max.y);

     var overlay_y = Math.min(overlay_der_y,overlay_izq_y)/Math.min(obj1_scale_y, obj2_scale_y);
   }else {
     var overlay_y = 0;
   }
   return overlay_x*overlay_y;

 },

}
