import numpy as np
import cv2
import argparse
import Augmentor
import ntpath

# ============================================================================

CANVAS_SIZE = (900,1200)

FINAL_LINE_COLOR = (0, 255, 255)
WORKING_LINE_COLOR = (255, 0, 0)

# ============================================================================
# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image to be scanned")
args = vars(ap.parse_args())

class PolygonDrawer(object):
	def __init__(self, window_name):
		self.window_name = window_name # Name for our window

		self.done = False # Flag signalling we're done
		self.current = (0, 0) # Current position, so we can draw the line-in-progress
		self.points = [] # List of points defining our polygon

	def on_mouse(self, event, x, y, buttons, user_param):
		# Mouse callback that gets called for every mouse event (i.e. moving, clicking, etc.)

		if self.done: # Nothing more to do
			return

		if event == cv2.EVENT_MOUSEMOVE:
			# We want to be able to draw the line-in-progress, so update current mouse position
			self.current = (x, y)
		elif event == cv2.EVENT_LBUTTONDOWN:
			# Left click means adding a point at current position to the list of points
			print("Adding point #%d with position(%d,%d)" % (len(self.points), x, y))
			self.points.append((x, y))
		elif event == cv2.EVENT_RBUTTONDOWN:
			# Right click means we're done
			print("Completing polygon with %d points." % len(self.points))
			self.done = True


	def run(self):
		# Let's create our working window and set a mouse callback to handle events
		cv2.namedWindow(self.window_name) #, flags=cv2.CV_WINDOW_AUTOSIZE)
		image = cv2.imread(args["image"])
		height, width, channels = image.shape 
		CANVAS_SIZE = (height, width)
		#image = cv2.imread( "..\document-scanner\images\page.jpg")
		#image = cv2.resize( image, CANVAS_SIZE)
		output = image.copy()
		cv2.imshow(self.window_name, image)
		cv2.setMouseCallback(self.window_name, self.on_mouse)

		while(not self.done):
			# This is our drawing loop, we just continuously draw new images
			# and show them in the named window
			canvas = output ##np.zeros(CANVAS_SIZE, np.uint8)
			if (len(self.points) > 0):
				# Draw all the current polygon segments
				cv2.polylines(canvas, np.array([self.points]), False, FINAL_LINE_COLOR, 1)
				# And  also show what the current segment would look like
				cv2.line(canvas, self.points[-1], self.current, WORKING_LINE_COLOR)
			# Update the window
			##cv2.imshow(self.window_name, canvas)
			cv2.addWeighted(image, 0.5, canvas, 1 - 0.5, 0, output)
			cv2.imshow(self.window_name, output)
			# And wait 50ms before next iteration (this will pump window messages meanwhile)
			if cv2.waitKey(50) == 27: # ESC hit
				self.done = True

		# User finised entering the polygon points, so let's make the final drawing
		canvas = output ##np.zeros(CANVAS_SIZE, np.uint8)
		# of a filled polygon
		if (len(self.points) > 0):
			cv2.polylines(canvas, np.array([self.points]), True, FINAL_LINE_COLOR, 5)
		# And show it
		cv2.imshow(self.window_name, canvas)
		# Waiting for the user to press any key
		cv2.waitKey()

		cv2.destroyWindow(self.window_name)
		return image

# ============================================================================
def four_point_transform(image, pts):
	# obtain a consistent order of the points and unpack them
	# individually
	rect = pts #order_points(pts)
	(tl, tr, br, bl) = rect

	# compute the width of the new image, which will be the
	# maximum distance between bottom-right and bottom-left
	# x-coordiates or the top-right and top-left x-coordinates
	widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
	widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
	maxWidth = max(int(widthA), int(widthB))

	# compute the height of the new image, which will be the
	# maximum distance between the top-right and bottom-right
	# y-coordinates or the top-left and bottom-left y-coordinates
	heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
	heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
	maxHeight = max(int(heightA), int(heightB))

	# now that we have the dimensions of the new image, construct
	# the set of destination points to obtain a "birds eye view",
	# (i.e. top-down view) of the image, again specifying points
	# in the top-left, top-right, bottom-right, and bottom-left
	# order
	dst = np.array([
		[0, 0],
		[maxWidth - 1, 0],
		[maxWidth - 1, maxHeight - 1],
		[0, maxHeight - 1]], dtype = "float32")

	src = np.array([
		tl,
		tr,
		br,
		bl], dtype = "float32")

	# compute the perspective transform matrix and then apply it
	M = cv2.getPerspectiveTransform(src, dst)
	warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

	# return the warped image
	return warped
	
# ============================================================================
def augmentate(path_base):
	p = Augmentor.Pipeline(path_base)
	# Point to a directory containing ground truth data.
	# Images with the same file names will be added as ground truth data
	# and augmented in parallel to the original data.
	p.ground_truth("images\base")
	# Add operations to the pipeline as normal:
	#p.rotate(probability=1, max_left_rotation=5, max_right_rotation=5)
	#p.flip_left_right(probability=0.5)
	p.zoom_random(probability=0.5, percentage_area=0.2)
	#p.flip_top_bottom(probability=0.5)
	p.random_distortion(probability=0.5, grid_width=10, grid_height=10, magnitude=3)
	p.sample(50)
	
def path_leaf(path):
	head, tail = ntpath.split(path)
	return tail,ntpath.basename(head)
	
# python  gus_gentool.py -i images\00750105534900L.jpg
if __name__ == "__main__":
	# load the image and compute the ratio of the old height
	# to the new height, clone it, and resize it
	
	pd = PolygonDrawer("Polygon")
	image = pd.run()
	
	print("Polygon = %s" % pd.points)
	warped = four_point_transform(image, pd.points)
	cv2.imshow("polygon.png2", warped)
	filename, tail=path_leaf(args["image"])
	print("filename=",tail,", name=",filename)
	path_base='.\\'+filename #path_base=tail+'\\base\\'+filename
	print("path base=", path_base)
	cv2.imwrite(path_base, warped)
	cv2.waitKey(0)
	#augmentate(path_base)


	
	