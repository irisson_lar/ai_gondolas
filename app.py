#!flask/bin/python

import sys

from flask import Flask, render_template, request, redirect, Response
import random, json

import glob
from os.path import basename
from os import rename, listdir
import os

import xml.etree.cElementTree as ET

from base64 import b64decode

fileIndex = 0


app = Flask(__name__, static_url_path='/static')


@app.route('/')
def output():
	# serve index template
	return render_template('index3.html', name='index3')


@app.route('/receiver', methods = ['POST'])
def dataReceiver():
	print ("receiver(render %d): ...",fileIndex)
	# read json + reply
	data = request.get_json()
	#print(data)
	writeXML(data)
	result=""
	return result

#def	writeXML(filename_image, bbox_base):
def writeXML(data):
	global fileIndex
	'''
	data["imgId"]
	data["imgScene"]
	data["bboxes"][i]["objClass"]
	data["bboxes"][i]["pixelCoordCenterX"]
	data["bboxes"][i]["pixelCoordCenterY"]
	data["bboxes"][i]["pixelCoordScaleW"]
	data["bboxes"][i]["pixelCoordScaleH"]
	'''
	path=".//dataset//"
	xmlfilename=str(fileIndex) +".xml"
	filename=str(fileIndex)+".png"
	#imgSize=data['imgSize']['w']
	width=data['imgSize']['w']
	height=data['imgSize']['h']
	#boxes, labelobj, xmlfilename, filename, path, width, height):
	root = ET.Element("annotation")
	#doc = ET.SubElement(root, "doc")
	ET.SubElement(root, "folder", ).text = "images"
	ET.SubElement(root, "filename", ).text = filename
	ET.SubElement(root, "path", ).text = "/home/irisson/repos/Ai_Gondolas/dataset/"+filename

	source = ET.SubElement(root, "source")
	ET.SubElement(source, "database", ).text = "Unknown"

	size = ET.SubElement(root, "size")
	ET.SubElement(size, "width", ).text = str(width)
	ET.SubElement(size, "height", ).text = str(height)
	ET.SubElement(size, "depth", ).text = "3"

	ET.SubElement(root, "segmented", ).text = "0"
	#{ bboxes : [], imgScene: img.src, imgId: indexNum };
	for items in data["bboxes"]:
		object = ET.SubElement(root, "object")
		ET.SubElement(object, "name", ).text = items['objClass'] #labelobj
		ET.SubElement(object, "pose", ).text = "Unspecified"
		ET.SubElement(object, "truncated", ).text = "0"
		ET.SubElement(object, "difficult", ).text = "0"

		bndbox = ET.SubElement(object, "bndbox")
		ET.SubElement(bndbox, "xmin", ).text = str(int(items['pixelCoordCenterX']))
		ET.SubElement(bndbox, "ymin", ).text = str(int(items['pixelCoordCenterY']))
		ET.SubElement(bndbox, "xmax", ).text = str(int(items['pixelCoordScaleW']))
		ET.SubElement(bndbox, "ymax", ).text = str(int(items['pixelCoordScaleH']))
	tree = ET.ElementTree(root)
	filenametmp=path+xmlfilename
	#print("[INFO] filenametmp=", filenametmp, ", xmlfilename=",xmlfilename,", path="+path," xmin=",boxes[0], " ymin=",boxes[1]," xmax=",boxes[2]," ymax=",boxes[3])
	tree.write(filenametmp)
	data_uri=data['imgScene']
	#print(data_uri)
	encoded = data_uri.split(",", 1)[1]
	data2 = b64decode(encoded)
	imgtmpfilename=path+filename
	with open(imgtmpfilename, "wb") as f:
		f.write(data2)
	fileIndex+=1


if __name__ == '__main__':
	# run!
	app.run()
